package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.Hardware.Deposit;
import org.firstinspires.ftc.teamcode.Hardware.DriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Intake;
import org.firstinspires.ftc.teamcode.Hardware.OdometryWheel;
import org.firstinspires.ftc.teamcode.Utils.BulkReadHandler;
import org.firstinspires.ftc.teamcode.Utils.State;
import org.openftc.revextensions2.ExpansionHubEx;

@TeleOp(name="OdomTest", group="TeleOp")
public class OdomtryTesting extends LinearOpMode
{

    private DriveTrain dt;
    private Intake intake;
    private State state;


    @Override
    public void runOpMode()
    {
        state = State.DRIVING;
        dt = new DriveTrain(this, false);
        intake = new Intake(this);

        BulkReadHandler bulk = new BulkReadHandler(this);

        waitForStart();

        while(opModeIsActive())
        {

            bulk.readData();

            dt.track(bulk);

            double x = gamepad1.left_stick_x;
            double y = -gamepad1.left_stick_y;
            double turn = gamepad1.right_stick_x;

            double r = Math.sqrt((x * x) + (y * y));
            double theta = Math.atan2(y, x);

            intake.power(gamepad1.right_trigger - gamepad1.left_trigger);

            dt.drive(r, theta, turn, state);

            telemetry.clear();
            telemetry.addLine("Hertz: " + bulk.getLastTickrate());
            telemetry.addLine("x: " + dt.getX());
            telemetry.addLine("y: " + dt.getY());
            telemetry.addLine("t (deg): " + Math.toDegrees(dt.getTheta()));
            telemetry.update();
        }

    }

}
