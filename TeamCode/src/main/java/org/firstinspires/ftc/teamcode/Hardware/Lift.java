package org.firstinspires.ftc.teamcode.Hardware;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

public class Lift
{

    private Motor left;
    private Motor right;
    private double targetPosition;
    private double maxPower;
    private boolean isTele;

    public Lift(OpMode op, boolean isTele)
    {
        left = new Motor("liftL", op);
        right = new Motor("liftR", op);

        left.setRunMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        right.setRunMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        targetPosition = 0;

        left.setTargetPosition(0);
        right.setTargetPosition(0);

        left.setPower(0);
        right.setPower(0);

        left.setConstants(DcMotor.RunMode.RUN_TO_POSITION, DcMotor.ZeroPowerBehavior.BRAKE, DcMotor.Direction.REVERSE);
        right.setConstants(DcMotor.RunMode.RUN_TO_POSITION, DcMotor.ZeroPowerBehavior.BRAKE, DcMotor.Direction.FORWARD);

        this.isTele = isTele;
    }

    public void tick(OpMode op)
    {
        op.telemetry.addData("Target", targetPosition);
        op.telemetry.addData("Left", left.getCurrentPosition());
        op.telemetry.addData("Right", right.getCurrentPosition());
    }

    public void setTargetPosition(double targetPosition){
        left.setTargetPosition((int) targetPosition);
        right.setTargetPosition((int) targetPosition);
        this.targetPosition = targetPosition;
    }

    public double getTargetPosition(){
        return targetPosition;
    }

    public void setPower(double power){
        if(!isTele){
            return;
        }
        left.setPower(power);
        right.setPower(power);
        this.maxPower = power;
    }

    public double getCurrentPosition(){
        return left.getCurrentPosition();
    }
}
