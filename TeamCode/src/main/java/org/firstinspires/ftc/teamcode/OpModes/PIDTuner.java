package org.firstinspires.ftc.teamcode.OpModes;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.Hardware.DriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Intake;
import org.firstinspires.ftc.teamcode.Hardware.Lift;
import org.firstinspires.ftc.teamcode.Utils.BulkReadHandler;
import org.firstinspires.ftc.teamcode.Utils.MovementPoint;
import org.firstinspires.ftc.teamcode.Utils.PIDF;
import org.firstinspires.ftc.teamcode.Utils.RobotMovement;
import org.firstinspires.ftc.teamcode.Utils.State;

/*
 * Sine wave sample to demonstrate telemetry and config variables in action. Adjust the amplitude,
 * phase, and frequency of the oscillation and watch the changes propagate immediately to the graph.
 */
@Config
@TeleOp
public class PIDTuner extends LinearOpMode {
    public static double P = .006;
    public static double I = 0;
    public static double D = 0;
    public static double F = 0;
    public static double target = 0;

    private Lift lift;

    @Override
    public void runOpMode() throws InterruptedException {
        FtcDashboard dashboard = FtcDashboard.getInstance();
        telemetry = dashboard.getTelemetry();

        double lastP = P;
        double lastD = D;
        double lastI = I;
        double lastF = F;

        lift = new Lift(this, true);

        BulkReadHandler bulk = new BulkReadHandler(this);

        waitForStart();

        if (isStopRequested()) return;

        while (opModeIsActive()) {
            bulk.readData();

            if(P != lastP || I != lastI || D != lastD || F != lastF);
            {

            }

            telemetry.update();

            lastP = P;
            lastD = D;
            lastI = I;
            lastF = F;

            lift.setTargetPosition(target);
        }
    }
}