package org.firstinspires.ftc.teamcode.OpModes;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.Hardware.DriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Intake;
import org.firstinspires.ftc.teamcode.Utils.BulkReadHandler;
import org.firstinspires.ftc.teamcode.Utils.State;

/*
 * Sine wave sample to demonstrate telemetry and config variables in action. Adjust the amplitude,
 * phase, and frequency of the oscillation and watch the changes propagate immediately to the graph.
 */
@Config
@TeleOp
public class TrackWidthTuner extends LinearOpMode {
    public static int readRate = 5;
    public static double REAR_DIST = 54;
    public static double TRACK_WIDTH = 364.5;

    private DriveTrain dt;
    private Intake intake;
    private State state;

    @Override
    public void runOpMode() throws InterruptedException {
        FtcDashboard dashboard = FtcDashboard.getInstance();
        telemetry = dashboard.getTelemetry();

        state = State.DRIVING;
        dt = new DriveTrain(this, false);
        intake = new Intake(this);

        BulkReadHandler bulk = new BulkReadHandler(this);

        waitForStart();

        if (isStopRequested()) return;

        while (opModeIsActive()) {
            bulk.readData();

            double x = gamepad1.left_stick_x;
            double y = -gamepad1.left_stick_y;
            double turn = gamepad1.right_stick_x;

            if(gamepad1.a){
                dt.setPos(0, 0, 0);
            }

            double r = Math.sqrt((x * x) + (y * y));
            double theta = Math.atan2(y, x);

            intake.power(gamepad1.right_trigger - gamepad1.left_trigger);
            dt.drive(r, theta, turn, state);

            dt.track(bulk, readRate, REAR_DIST, TRACK_WIDTH, telemetry);
            telemetry.addData("x", dt.getX());
            telemetry.addData("y", dt.getY());
            telemetry.addData("theta", Math.toDegrees(dt.getTheta()));
            telemetry.addData("deltaT", bulk.getLastDeltaT() / 1000000);
            telemetry.update();
        }
    }
}