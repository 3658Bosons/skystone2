package org.firstinspires.ftc.teamcode.OpModes;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.Hardware.DriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Intake;
import org.firstinspires.ftc.teamcode.Utils.BulkReadHandler;
import org.firstinspires.ftc.teamcode.Utils.MovementPoint;
import org.firstinspires.ftc.teamcode.Utils.RobotMovement;
import org.firstinspires.ftc.teamcode.Utils.State;

/*
 * Sine wave sample to demonstrate telemetry and config variables in action. Adjust the amplitude,
 * phase, and frequency of the oscillation and watch the changes propagate immediately to the graph.
 */
@Config
@TeleOp
public class ServoTuner2 extends LinearOpMode {
    public static double a = .7;
    public static double b = .58;
    public static double y = .43;

    private Servo servo;

    @Override
    public void runOpMode() throws InterruptedException {
        FtcDashboard dashboard = FtcDashboard.getInstance();
        telemetry = dashboard.getTelemetry();

        servo = hardwareMap.get(Servo.class, "Stop");

        waitForStart();

        if (isStopRequested()) return;

        while(opModeIsActive())
        {
            if(gamepad1.a)
                servo.setPosition(a);
            else if(gamepad1.b)
                servo.setPosition(b);
            else if(gamepad1.y){
                servo.setPosition(y);
            }
        }
    }
}