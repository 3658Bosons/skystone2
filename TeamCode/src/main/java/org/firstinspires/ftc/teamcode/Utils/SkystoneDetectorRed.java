package org.firstinspires.ftc.teamcode.Utils;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvInternalCamera;
import org.openftc.easyopencv.OpenCvPipeline;

public class SkystoneDetectorRed
{

    private OpenCvCamera cam;
    private OpMode op;
    private PipelineRed pipeline;
    private String pipe;

    public SkystoneDetectorRed(OpMode op)
    {
        this.op = op;
        this.pipe = pipe;
        this.pipeline = new PipelineRed(op);

        int cameraMonitorViewId = op.hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId",
                "id", op.hardwareMap.appContext.getPackageName());
        cam = new OpenCvInternalCamera(OpenCvInternalCamera.CameraDirection.BACK, cameraMonitorViewId);

        cam.openCameraDevice();
        cam.setPipeline(pipeline);
        cam.startStreaming(640, 480, OpenCvCameraRotation.UPRIGHT);
    }

    public void stopStreaming(){
        cam.stopStreaming();
    }

    public void pauseViewport(){
        cam.pauseViewport();
    }

    public void resumeViewport(){
        cam.resumeViewport();
    }

    public int getDecision(){
        return pipeline.getDecision();
    }

}

class PipelineRed extends OpenCvPipeline
{

    OpMode op;

    public PipelineRed(OpMode op){
        this.op = op;
    }

    private Color left = new Color();
    private Color mid = new Color();
    private Color right = new Color();

    private static final float w = 640f;
    private static final float h = 480f;

    private static final Point leftTL = new Point(w*(16f/64f), h*(17f/32f));
    private static final Point leftBR = new Point(w*(21f/64f), h*(19f/32f));

    private static final Point rightTL = new Point(w*(40/64f), h*(17f/32f));
    private static final Point rightBR = new Point(w*(45/64f), h*(19f/32f));

    private static final Point midTL = new Point(w*(28/64f), h*(17f/32f));
    private static final Point midBR = new Point(w*(33/64f), h*(19f/32f));

    @Override
    public void onViewportTapped()
    {

    }

    @Override
    public Mat processFrame(Mat input)
    {

        Imgproc.rectangle(input, leftTL, leftBR, new Scalar(0, 255, 0), 2);
        Imgproc.rectangle(input, midTL, midBR, new Scalar(0, 255, 0), 2);
        Imgproc.rectangle(input, rightTL, rightBR, new Scalar(0, 255, 0), 2);

        process(leftTL, leftBR, input, left);
        process(midTL, midBR, input, mid);
        process(rightTL, rightBR, input, right);

        tellemData();

        return input;
    }

    public Color getLeft(){
        return left;
    }
    public Color getMid(){
        return mid;
    }
    public Color getRight(){
        return right;
    }

    public int getDecision(){
        int l = (left.getR() + left.getG()) / 2;
        int m = (mid.getR() + mid.getG()) / 2;
        int r = (right.getR() + right.getG()) / 2;

        if(l < m && l < r){
            return 0;
        }else if(m < l && m < r){
            return 1;
        }else if(r < l && r < m){
            return 2;
        }else{
            return 0;
        }
    }

    public void process(Point p1, Point p2, Mat input, Color c)
    {
        int r = 0;
        int g = 0;
        int b = 0;
        int tot = 0;

        for(int x = (int)p1.x; x<(int)p2.x; x++)
        {
            for(int y = (int)p1.y; y<(int)p2.y; y++)
            {
                r += input.get(y, x)[0];
                g += input.get(y, x)[1];
                b += input.get(y, x)[2];
                tot++;
            }
        }
        r/=tot;
        g/=tot;
        b/=tot;


        c.setR(r);
        c.setG(g);
        c.setB(b);
    }

    private void tellemData(){
        op.telemetry.clear();
        op.telemetry.addLine("Left:  R" + left.getR() + " G " + left.getG() + " B " + left.getB());
        op.telemetry.addLine("Middle:  R" + mid.getR() + " G " + mid.getG() + " B " + mid.getB());
        op.telemetry.addLine("Right:  R" + right.getR() + " G " + right.getG() + " B " + right.getB());
        op.telemetry.addLine("Decision: " + getDecision());
        op.telemetry.update();
    }
}