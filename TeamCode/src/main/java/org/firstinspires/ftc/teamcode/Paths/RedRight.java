package org.firstinspires.ftc.teamcode.Paths;

import com.acmerobotics.dashboard.config.Config;

import org.firstinspires.ftc.teamcode.Utils.MovementPoint;
import org.firstinspires.ftc.teamcode.Utils.PurePursuit;

import java.util.ArrayList;

@Config
public class RedRight
{

    private ArrayList<PurePursuit> paths;

    public RedRight()
    {
        paths = new ArrayList();

        ArrayList<MovementPoint> points = new ArrayList();
        points.add(new MovementPoint(3435, 980, 0));
        points.add(new MovementPoint(2850, 1500, Math.toRadians(-180), 0));
        points.add(new MovementPoint(2200, 1340, Math.toRadians(-150), 50));
        paths.add(new PurePursuit(points));

        ArrayList<MovementPoint> points2 = new ArrayList();
        points2.add(new MovementPoint(2200, 1300, 0));
        points2.add(new MovementPoint(2675, 1200, Math.toRadians(-45), 0));
        points2.add(new MovementPoint(2650, 2000, Math.toRadians(-90), 0));
        points2.add(new MovementPoint(2760, 2750, 0));
        points2.add(new MovementPoint(2325, 2900, Math.toRadians(-5), 60));
        paths.add(new PurePursuit(points2));

        ArrayList<MovementPoint> points3 = new ArrayList();
        points3.add(new MovementPoint(2325, 2900, 0));
        points3.add(new MovementPoint(2700, 2600, -Math.PI / 2, 0));
        points3.add(new MovementPoint(2725, 2000, -Math.PI / 2, 0));
        points3.add(new MovementPoint(2751, 1400, -Math.PI / 2, 0));
        points3.add(new MovementPoint(2750, 1000, 0));
        points3.add(new MovementPoint(2175, 780,  Math.toRadians(-125), 50));
        paths.add(new PurePursuit(points3));

        ArrayList<MovementPoint> points4 = new ArrayList();
        points4.add(new MovementPoint(2175, 800, 0));
        points4.add(new MovementPoint(2625, 1000, -Math.toRadians(135), 0));
        points4.add(new MovementPoint(2650, 2600, -Math.toRadians(90), 0));
        points4.add(new MovementPoint(3000, 2950, -Math.toRadians(90), 50));
        paths.add(new PurePursuit(points4));

        ArrayList<MovementPoint> points5 = new ArrayList();
        points5.add(new MovementPoint(3000, 2950, 0));
        points5.add(new MovementPoint(2700, 2800, -Math.PI / 2, 0));
        points5.add(new MovementPoint(2725, 2000, -Math.PI / 2, 0));
        points5.add(new MovementPoint(2750, 800, -Math.PI / 2, 0));
        points5.add(new MovementPoint(2825, 550, -Math.PI / 2, 0));
        points5.add(new MovementPoint(2100, 340,  Math.toRadians(-150), 50));
        points5.add(new MovementPoint(2050, 330,  Math.toRadians(-100), 50));
        paths.add(new PurePursuit(points5));

        ArrayList<MovementPoint> points6 = new ArrayList();
        points6.add(new MovementPoint(2050, 330, 0));
        points6.add(new MovementPoint(2700, 1000, -Math.toRadians(90), 0));
        points6.add(new MovementPoint(2699, 2000, -Math.toRadians(90), 0));
        points6.add(new MovementPoint(2500, 2900, Math.toRadians(-90), 50));
        paths.add(new PurePursuit(points6));

        ArrayList<MovementPoint> points7 = new ArrayList();
        points7.add(new MovementPoint(2600, 2800, 0));
        points7.add(new MovementPoint(2900, 2600, Math.toRadians(-90), 0));
        points7.add(new MovementPoint(2750, 1900, Math.toRadians(-90), 50));
        paths.add(new PurePursuit(points7));
    }

    public ArrayList<PurePursuit> getPaths(){
        return paths;
    }

}
