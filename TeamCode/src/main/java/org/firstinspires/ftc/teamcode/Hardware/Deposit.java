package org.firstinspires.ftc.teamcode.Hardware;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.Utils.State;

@Config
public class Deposit {
    public static double release = .58;
    private static double cap = 0;
    private static double gripping = 1;
    private static double idle = .9;

    private static double rotStraight = .054;
    private static double rotHorizontal = .385;

    private static double ticksPerLevel = 125;

    private static double STOP_UP = 0;
    private static double STOP_DOWN = .35;

    public static double LEFT_IN = .05;
    public static double LEFT_OUT = .38;
    public static double RIGHT_IN = .58;
    public static double RIGHT_OUT = .25;

    private int level = 0;

    private Lift lift;

    private Servo leftLinkage;
    private Servo rightLinkage;
    private Servo gripper;
    private Servo gripperRotator;
    private Servo stop;

    private State lastState;
    private boolean releasing;

    private boolean rotate;
    private boolean isTele;

    private static LinearOpMode op;

    public Deposit(LinearOpMode op, State state, boolean isTele)
    {
        leftLinkage = op.hardwareMap.get(Servo.class, "LinkL");
        rightLinkage = op.hardwareMap.get(Servo.class, "LinkR");
        gripper = op.hardwareMap.get(Servo.class, "Gripper");
        gripperRotator = op.hardwareMap.get(Servo.class, "Rot");
        stop = op.hardwareMap.get(Servo.class, "Stop");

        lift = new Lift(op, isTele);
        this.isTele = isTele;

        lastState = state;

        releasing = false;

        this.op = op;

        rotate = false;

        setArmPos(0);
        idleStone();
        gripperRotator.setPosition(rotStraight);
        stop.setPosition(STOP_UP);
    }

    public boolean tick(double increment, State state)
    {
        op.telemetry.clear();
        lift.tick(op);
        switch(state)
        {
            case DRIVING:
                if(lastState != State.DRIVING){
                    setArmPos(0);
                    releaseStone();
                    stop.setPosition(STOP_UP);
                }
                break;
            case LOCKED:
                if(lastState != State.LOCKED){
                    stop.setPosition(STOP_DOWN);
                    grabStone();
                }
                break;
            case LIFTING:
                if(lastState != State.LIFTING){
                    lift.setPower(1);
                    lift.setTargetPosition(level * ticksPerLevel);
                }
                lift.setTargetPosition(lift.getTargetPosition() + increment);
                break;
            case DEPOSITING:
                if(lastState != State.DEPOSITING){
                    grabBlockThread();
                    lift.setPower(.3);
                }
                lift.setTargetPosition(lift.getTargetPosition() + increment);
                break;
            case RETRACTING:
                if(lastState != State.RETRACTING){
                    releasing = true;
                    releaseBlockThread();
                    level++;
                }
                if(!releasing) {
                    rotate = false;
                    lift.setPower(.4);
                    lift.setTargetPosition(-30);
                }
                if(lift.getCurrentPosition() < 6 && !releasing){
                    lift.setPower(0);
                    return true;
                }
                break;
        }

        lastState = state;

        op. telemetry.addLine("Level: " + level);
        op. telemetry.addLine("Rotate: " + rotate);
        op.telemetry.update();

        return false;
    }

    public void grabStone()
    {
        gripper.setPosition(gripping);
    }

    public void releaseStone()
    {
        gripper.setPosition(release);
    }

    private void idleStone()
    {
        gripper.setPosition(idle);
    }

    public void releaseCap()
    {
        gripper.setPosition(cap);
    }

    private void rotateBlock(int dir)
    {
        if(dir == 0)
        {
            gripperRotator.setPosition(rotStraight);
        }
        else if(dir == 1)
        {
            gripperRotator.setPosition(rotHorizontal);
        }
    }

    public void setRotate(boolean rotate){
        this.rotate = rotate;
    }

    public boolean getRotate(){
        return rotate;
    }

    public void incLevel(int amt){
        level += amt;
        if(level > 13){
            level = 0;
        }else if(level < 0){
            level = 13;
        }
    }

    public void releaseBlockThread()
    {
        Thread t = new Thread(releaseBlock);

        t.start();
    }

    public void setIdle() {
        setArmPos(0);
    }

    public void grabBlockThread()
    {
        Thread t = new Thread(grabBlock);
        t.start();
    }

    public void capStoneThread()
    {
        Thread t = new Thread(capStone);
        t.start();
    }

    public void fullDep(){
        Thread t = new Thread(fullDep);
        t.start();
    }

    public void setArmPos(int pos)
    {
        if(pos == 0)
        {
            leftLinkage.setPosition(LEFT_IN);
            rightLinkage.setPosition(RIGHT_IN);
        }
        else if(pos == 1)
        {
            leftLinkage.setPosition(LEFT_OUT);
            rightLinkage.setPosition(RIGHT_OUT);
        }
    }

    private Runnable grabBlock = new Runnable()
    {
      public void run()
      {
          setArmPos(1);
          if(rotate){
              op.sleep(500);
              rotateBlock(1);
          }
      }
    };

    private Runnable fullDep = new Runnable()
    {
        public void run()
        {
            grabStone();
            stop.setPosition(STOP_DOWN);
            op.sleep(400);
            setArmPos(1);
        }

    };

    private Runnable releaseBlock = new Runnable()
    {
        @Override
        public void run()
        {
            releaseStone();
            op.sleep(300);
            if(rotate){
                rotateBlock(0);
                op.sleep(500);
            }
            if(isTele){
                lift.setTargetPosition(lift.getCurrentPosition() + 75);
                op.sleep(250);
            }
            setArmPos(0);
            op.sleep(250);
            setArmPos(1);
            op.sleep(10);
            setArmPos(0);
            op.sleep(250);
            releasing = false;
        }
    };

    private Runnable capStone = new Runnable()
    {
        public void run()
        {
            releaseCap();
            op.sleep(500);
            releaseStone();
        }
    };
}
