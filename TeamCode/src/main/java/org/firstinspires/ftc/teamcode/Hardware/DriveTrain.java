package org.firstinspires.ftc.teamcode.Hardware;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.Hardware.Motor;
import org.firstinspires.ftc.teamcode.Utils.BulkReadHandler;
import org.firstinspires.ftc.teamcode.Utils.PIDF;
import org.firstinspires.ftc.teamcode.Utils.State;

public class DriveTrain
{

    private Motor fl;
    private Motor bl;
    private Motor fr;
    private Motor br;

    private OdometryWheel odoL;
    private OdometryWheel odoM;
    private OdometryWheel odoR;

    private Servo leftServo;
    private Servo rightServo;

    private double angleOffset;

    private static double leftUp = .15;
    private static double leftIdle = .64;
    private static double leftDown = .73;

    private static double rightUp = .9;
    private static double rightIdle = .44;
    private static double rightDown = .34;

    private static double TRACK_WIDTH = 365.95;
    private static double REAR_DIST = 65;

    private double theta;
    private double x;
    private double y;

    private static double antiSpamScrub = 0;

    private double lastFL;
    private double lastBL;
    private double lastFR;
    private double lastBR;

    private Gyro gyro;
    //help im trapped within cyberspace I dont know where this message will appear but whatever you do DO NOT SPELL ICUP
    public DriveTrain(OpMode op, boolean istele)
    {
        fl = new Motor("fl", op);
        bl = new Motor("bl", op);
        fr = new Motor("fr", op);
        br = new Motor("br", op);

        odoL = new OdometryWheel(true);
        odoM = new OdometryWheel(false);
        odoR = new OdometryWheel(false);

        if(!istele)
            gyro = new Gyro(op);

        leftServo = op.hardwareMap.get(Servo.class, "FoundationL");
        rightServo = op.hardwareMap.get(Servo.class, "FoundationR");

        leftServo.setPosition(leftUp);
        rightServo.setPosition(rightUp);
        //the year is 3030
        fl.resetEncoder();
        bl.resetEncoder();
        br.resetEncoder();
        fr.resetEncoder();

        fl.setConstants(DcMotor.RunMode.RUN_WITHOUT_ENCODER, DcMotor.ZeroPowerBehavior.BRAKE, DcMotorSimple.Direction.FORWARD);
        bl.setConstants(DcMotor.RunMode.RUN_WITHOUT_ENCODER, DcMotor.ZeroPowerBehavior.BRAKE, DcMotorSimple.Direction.FORWARD);
        fr.setConstants(DcMotor.RunMode.RUN_WITHOUT_ENCODER, DcMotor.ZeroPowerBehavior.BRAKE, DcMotorSimple.Direction.REVERSE);
        br.setConstants(DcMotor.RunMode.RUN_WITHOUT_ENCODER, DcMotor.ZeroPowerBehavior.BRAKE, DcMotorSimple.Direction.REVERSE);

        lastFL = 0;
        lastBL = 0;
        lastFR = 0;
        lastBR = 0;

        angleOffset = 0;
    }

    public void drive(double power, double theta, double turn, State state)
    {//vroom

        double x = power * Math.cos(theta) * 1.5;
        double y = power * Math.sin(theta);

        double flPower = y + x + turn;
        double blPower = y - x + turn;
        double frPower = y - x - turn;
        double brPower = y + x - turn;

        double maxPower = 1;
        switch(state)
        {
            case DRIVING:
            case LOCKED:
                maxPower =1;
                break;
            case DEPOSITING:
            case RETRACTING:
                maxPower = .5;
                break;
        }

        if(Math.abs(flPower) > maxPower || Math.abs(blPower) > maxPower
                || Math.abs(frPower) > maxPower || Math.abs(brPower) > maxPower)
        {
            double max = Math.max(Math.abs(flPower), Math.abs(blPower));
            max = Math.max(max, Math.abs(frPower));
            max = Math.max(max, Math.abs(brPower));

            flPower /= max;
            blPower /= max;
            frPower /= max;
            brPower /= max;
        }

        fl.setPower(flPower);
        bl.setPower(blPower);
        fr.setPower(frPower);
        br.setPower(brPower);
    }

    public void track(BulkReadHandler bulk) {//google the five eyes
        odoL.tick(bulk.getLeftOdomPos());
        odoM.tick(bulk.getMiddleOdomPos());
        odoR.tick(bulk.getRightOdomPos());

        double dTheta = Math.toRadians(gyro.getAngle());
        double ldX = odoM.getDeltaMM() - REAR_DIST * (dTheta - theta);
        double ldY = (odoR.getDeltaMM() + odoL.getDeltaMM()) / 2;

        double mTheta = (theta + dTheta) / 2;
        x += ldY * Math.cos(mTheta) + ldX * Math.sin(mTheta);
        y += ldY * Math.sin(mTheta) - ldX * Math.cos(mTheta);
        theta = dTheta;

        if (bulk.getTicksFromStart() % 25 == 0) {
            //theta = Math.toRadians(gyro.getAngle());
        }
    }

    public void track(BulkReadHandler bulk, int readRate, double REAR_DIST, double TRACK_WIDTH, Telemetry telem)
    {//google the five eyes
        odoL.tick(bulk.getLeftOdomPos());
        odoM.tick(bulk.getMiddleOdomPos());
        odoR.tick(bulk.getRightOdomPos());

        telem.addData("leftCounts", -bulk.getLeftOdomPos());
        telem.addData("midCounts", bulk.getMiddleOdomPos());
        telem.addData("rightCounts", bulk.getRightOdomPos());

        double dTheta = (odoR.getLastMM() - odoL.getLastMM()) / TRACK_WIDTH + angleOffset;
        double ldX = odoM.getDeltaMM() - REAR_DIST * (dTheta - theta);
        double ldY = (odoR.getDeltaMM() + odoL.getDeltaMM()) / 2;

        double mTheta = (theta + dTheta) / 2;
        x += ldY * Math.cos(mTheta) + ldX * Math.sin(mTheta);
        y += ldY * Math.sin(mTheta) - ldX * Math.cos(mTheta);
        theta = dTheta;

        if(bulk.getTicksFromStart() % readRate == 0){
            //theta = Math.toRadians(gyro.getAngle());
        }
    }

    public void setRunMode(DcMotor.RunMode mode)
    {
        fl.setRunMode(mode);
        bl.setRunMode(mode);
        br.setRunMode(mode);
        fr.setRunMode(mode);
    }

    public void grabFoundation(){
        leftServo.setPosition(leftDown);
        rightServo.setPosition(rightDown);
    }

    public void idleFoundation(){
        leftServo.setPosition(leftIdle);
        rightServo.setPosition(rightIdle);
    }

    public void releaseFoundation(){
        leftServo.setPosition(leftUp);
        rightServo.setPosition(rightUp);
    }

    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }
    public double getTheta(){
        return theta;
    }

    public void setPos(double x, double y, double theta)
    {
        this.x = x;
        this.y = y;
        this.angleOffset = theta;
        gyro.setAngle(Math.toDegrees(theta));

        setRunMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        setRunMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

}
