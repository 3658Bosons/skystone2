package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.robot.Robot;

import org.firstinspires.ftc.teamcode.Hardware.Deposit;
import org.firstinspires.ftc.teamcode.Hardware.DriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Intake;
import org.firstinspires.ftc.teamcode.Paths.RedLeft;
import org.firstinspires.ftc.teamcode.Paths.RedMiddle;
import org.firstinspires.ftc.teamcode.Paths.RedRight;
import org.firstinspires.ftc.teamcode.Utils.BulkReadHandler;
import org.firstinspires.ftc.teamcode.Utils.MovementPoint;
import org.firstinspires.ftc.teamcode.Utils.PurePursuit;
import org.firstinspires.ftc.teamcode.Utils.RobotMovement;
import org.firstinspires.ftc.teamcode.Utils.SkystoneDetectorRed;
import org.firstinspires.ftc.teamcode.Utils.State;

import java.util.ArrayList;

@Autonomous(name="REDAuto", group="Red Autonomous") //pp hehe
public class RedAuto extends LinearOpMode
{

    private DriveTrain dt;
    private State state;
    private Intake intake;
    private Deposit deposit;
    private int cPath;
    private int lP;
    private int skystonePos;
    private SkystoneDetectorRed detector;
    private ArrayList<PurePursuit> pathsM;
    private ArrayList<PurePursuit> pathsL;
    private ArrayList<PurePursuit> pathsR;

    @Override
    public void runOpMode()
    {
        state = State.DRIVING;
        dt = new DriveTrain(this, false);
        deposit = new Deposit(this, State.RETRACTING, false);
        intake = new Intake(this);
        dt.setPos(3435, 980, Math.PI);
        cPath = 0;
        lP = 0;
        skystonePos = 0;
        detector = new SkystoneDetectorRed(this);

        BulkReadHandler bulk = new BulkReadHandler(this);
        pathsM = new RedMiddle().getPaths();
        pathsL = new RedLeft().getPaths();
        pathsR = new RedRight().getPaths();

        waitForStart();

        skystonePos = detector.getDecision();
        detector.stopStreaming();

        while(opModeIsActive())
        {

            bulk.readData();

            dt.track(bulk);

            if(bulk.getTicksFromStart() % 2 == 0)
            {
                telemetry.clear();
                telemetry.addLine("Hertz: " + bulk.getLastTickrate());
                telemetry.addLine("x: " + dt.getX() + " y: " + dt.getY() + "t (deg)" + Math.toDegrees(dt.getTheta()));

                if(skystonePos == 0){
                    tickLeft();
                }
                else if(skystonePos == 1){
                    tickMiddle();
                }
                else{
                    tickRight();
                }

                telemetry.update();
            }
        }

    }

    private void tickLeft()
    {
        if(cPath == 0){
            if(pathsL.get(0).purePursuit(dt, state, this, .5, .5, 400, 0)) {
                cPath++;
            }
            if(pathsL.get(0).getCurrentPoint() == 2 && cPath == 0 && lP != 2){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
                intake.power(1);
            }
            lP = pathsL.get(0).getCurrentPoint();
        }
        if(cPath == 1){
            if(pathsL.get(1).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
                dt.grabFoundation();
                deposit.fullDep();
                sleep(500);
            }
            if(pathsL.get(1).getCurrentPoint() == 3 && cPath == 1 && lP != 3){
                dt.idleFoundation();
                intake.power(0);
                deposit.tick(0, State.LOCKED);
            }
            lP = pathsL.get(1).getCurrentPoint();
        }
        if(cPath == 2){
            double speed = 1;
            if(pathsL.get(2).getCurrentPoint() > 3)
                speed = .4;
            if(pathsL.get(2).purePursuit(dt, state, this, speed, speed, 400, 0)) {
                cPath++;
                sleep(300);
            }
            if(pathsL.get(2).getCurrentPoint() == 2 && cPath == 2 && lP != 2){
                deposit.releaseBlockThread();
            }
            if(pathsL.get(2).getCurrentPoint() == 3 && cPath == 2 && lP != 3)
            {
                dt.releaseFoundation();
            }
            if(pathsL.get(2).getCurrentPoint() == 4 && cPath == 2 && lP != 4){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
                intake.power(1);
            }
            lP = pathsL.get(2).getCurrentPoint();
        }
        if(cPath == 3){
            if(pathsL.get(3).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DEPOSITING);
                sleep(600);
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.RETRACTING);
                sleep(500);
            }
            if(pathsL.get(3).getCurrentPoint() == 2 && lP != 2){
                intake.unjam();
            }
            if(pathsL.get(3).getCurrentPoint() == 3 && lP != 3){
                intake.power(0);
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.LOCKED);
            }
            lP = pathsL.get(3).getCurrentPoint();
        }
        if(cPath == 4){
            double speed = 1;
            if(pathsL.get(4).getCurrentPoint() == 3)
                speed = .5;
            if(pathsL.get(4).purePursuit(dt, state, this, speed, speed, 400, 0)) {
                cPath++;
                sleep(250);
            }
            if(pathsL.get(4).getCurrentPoint() == 2)
                deposit.setArmPos(0);
            if(pathsL.get(4).getCurrentPoint() == 3 && lP != 3){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
                intake.power(1);
            }
            lP = pathsL.get(4).getCurrentPoint();
        }
        if(cPath == 5){
            if(pathsL.get(5).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DEPOSITING);
                sleep(600);
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.RETRACTING);
                sleep(500);
            }
            if(pathsL.get(5).getCurrentPoint() == 2 && lP != 2){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.LOCKED);
                intake.power(0);
            }
            lP = pathsL.get(5).getCurrentPoint();
        }
        if(cPath == 6){
            if(pathsL.get(6).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
            }
            if(pathsL.get(6).getCurrentPoint() == 2){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
            }
        }
    }

    private void tickMiddle()
    {
        if(cPath == 0){
            if(pathsM.get(0).purePursuit(dt, state, this, .5, .5, 400, 0)) {
                cPath++;
            }
            if(pathsM.get(0).getCurrentPoint() == 2 && cPath == 0 && lP != 2){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
                intake.power(1);
            }
            lP = pathsM.get(0).getCurrentPoint();
        }
        if(cPath == 1){
            if(pathsM.get(1).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
                dt.grabFoundation();
                deposit.fullDep();
                sleep(500);
            }
            if(pathsM.get(1).getCurrentPoint() == 3 && cPath == 1 && lP != 3){
                dt.idleFoundation();
                intake.power(0);
                deposit.tick(0, State.LOCKED);
            }
            lP = pathsM.get(1).getCurrentPoint();
        }
        if(cPath == 2){
            double speed = 1;
            if(pathsM.get(2).getCurrentPoint() > 3)
                speed = .4;
            if(pathsM.get(2).purePursuit(dt, state, this, speed, speed, 400, 0)) {
                cPath++;
            }
            if(pathsM.get(2).getCurrentPoint() == 2 && cPath == 2 && lP != 2){
                deposit.releaseBlockThread();
            }
            if(pathsM.get(2).getCurrentPoint() == 3 && cPath == 2 && lP != 3)
            {
                dt.releaseFoundation();
            }
            if(pathsM.get(2).getCurrentPoint() == 4 && cPath == 2 && lP != 4){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
                intake.power(1);
            }
            lP = pathsM.get(2).getCurrentPoint();
        }
        if(cPath == 3){
            if(pathsM.get(3).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DEPOSITING);
                sleep(600);
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.RETRACTING);
                sleep(500);
            }
            if(pathsM.get(3).getCurrentPoint() == 2 && lP != 2){
                intake.power(0);
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.LOCKED);
            }
            lP = pathsM.get(3).getCurrentPoint();
        }
        if(cPath == 4){
            double speed = 1;
            if(pathsM.get(4).getCurrentPoint() == 3)
                speed = .5;
            if(pathsM.get(4).purePursuit(dt, state, this, speed, speed, 400, 0)) {
                cPath++;
                sleep(250);
            }
            if(pathsM.get(4).getCurrentPoint() == 2)
                deposit.setArmPos(0);
            if(pathsM.get(4).getCurrentPoint() == 3 && lP != 3){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
                intake.power(1);
            }
            lP = pathsM.get(4).getCurrentPoint();
        }
        if(cPath == 5){
            if(pathsM.get(5).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
                intake.power(0);
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DEPOSITING);
                sleep(600);
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.RETRACTING);
                sleep(500);
            }
            if(pathsM.get(5).getCurrentPoint() == 3 && lP != 3){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.LOCKED);
                intake.power(-1);
            }
            lP = pathsM.get(5).getCurrentPoint();
        }
        if(cPath == 6){
            if(pathsM.get(6).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
            }
            if(pathsM.get(6).getCurrentPoint() == 2){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
            }
        }
    }

    public void tickRight()
    {
        if(cPath == 0){
            if(pathsR.get(0).purePursuit(dt, state, this, .5, .5, 400, 0)) {
                cPath++;
            }
            if(pathsR.get(0).getCurrentPoint() == 2 && cPath == 0 && lP != 2){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
                intake.power(1);
            }
            lP = pathsR.get(0).getCurrentPoint();
        }
        if(cPath == 1){
            if(pathsR.get(1).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
                dt.grabFoundation();
                deposit.fullDep();
                sleep(500);
            }
            if(pathsR.get(1).getCurrentPoint() == 3 && cPath == 1 && lP != 3){
                dt.idleFoundation();
                intake.power(0);
                deposit.tick(0, State.LOCKED);
            }
            lP = pathsR.get(1).getCurrentPoint();
        }
        if(cPath == 2){
            double speed = 1;
            if(pathsR.get(2).getCurrentPoint() > 3)
                speed = .4;
            if(pathsR.get(2).purePursuit(dt, state, this, speed, speed, 400, 0)) {
                cPath++;
            }
            if(pathsR.get(2).getCurrentPoint() == 2 && cPath == 2 && lP != 2){
                deposit.releaseBlockThread();
            }
            if(pathsR.get(2).getCurrentPoint() == 3 && cPath == 2 && lP != 3)
            {
                dt.releaseFoundation();
            }
            if(pathsR.get(2).getCurrentPoint() == 4 && cPath == 2 && lP != 4){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
                intake.power(1);
            }
            lP = pathsR.get(2).getCurrentPoint();
        }
        if(cPath == 3){
            if(pathsR.get(3).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DEPOSITING);
                sleep(600);
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.RETRACTING);
                sleep(500);
            }
            if(pathsR.get(3).getCurrentPoint() == 2 && lP != 2){
                intake.power(0);
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.LOCKED);
            }
            lP = pathsR.get(3).getCurrentPoint();
        }
        if(cPath == 4){
            double speed = 1;
            if(pathsR.get(4).purePursuit(dt, state, this, speed, speed, 400, 0)) {
                cPath++;
                sleep(250);
            }
            if(pathsR.get(4).getCurrentPoint() == 2)
                deposit.setArmPos(0);
            if(pathsR.get(4).getCurrentPoint() == 3 && lP != 3){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
                intake.power(1);
            }
            lP = pathsR.get(4).getCurrentPoint();
        }
        if(cPath == 5){
            if(pathsR.get(5).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
                intake.power(0);
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DEPOSITING);
                sleep(600);
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.RETRACTING);
                sleep(500);
            }
            if(pathsR.get(5).getCurrentPoint() == 2 && lP != 2){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.LOCKED);
                intake.unjam();
            }
            lP = pathsR.get(5).getCurrentPoint();
        }
        if(cPath == 6){
            if(pathsR.get(6).purePursuit(dt, state, this, 1, 1, 400, 1)) {
                cPath++;
            }
            if(pathsR.get(6).getCurrentPoint() == 2){
                deposit.tick(0, State.BUFFER);
                deposit.tick(0, State.DRIVING);
            }
        }
    }

}
