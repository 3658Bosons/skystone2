package org.firstinspires.ftc.teamcode.Paths;

import com.acmerobotics.dashboard.config.Config;

import org.firstinspires.ftc.teamcode.Utils.MovementPoint;
import org.firstinspires.ftc.teamcode.Utils.PurePursuit;

import java.util.ArrayList;

@Config
public class BlueRight
{

    private ArrayList<PurePursuit> paths;

    public BlueRight()
    {
        paths = new ArrayList();

        ArrayList<MovementPoint> points = new ArrayList();
        points.add(new MovementPoint(565, 980, 0));
        points.add(new MovementPoint(1100, 1480, Math.toRadians(0), 0));
        points.add(new MovementPoint(1700, 1320, Math.toRadians(-30), 75));
        paths.add(new PurePursuit(points));

        ArrayList<MovementPoint> points2 = new ArrayList();
        points2.add(new MovementPoint(1800, 1300, 0));
        points2.add(new MovementPoint(1100, 1200, Math.toRadians(-45), 0));
        points2.add(new MovementPoint(1250, 2000, Math.toRadians(-90), 0));
        points2.add(new MovementPoint(1200, 2750, 0));
        points2.add(new MovementPoint(1500, 2900, Math.toRadians(-175), 60));
        paths.add(new PurePursuit(points2));

        ArrayList<MovementPoint> points3 = new ArrayList();
        points3.add(new MovementPoint(1500, 2900, 0));
        points3.add(new MovementPoint(1200, 2600, -Math.PI / 2, 0));
        points3.add(new MovementPoint(1175, 2000, -Math.PI / 2, 0));
        points3.add(new MovementPoint(1151, 1300, -Math.PI / 2, 0));
        points3.add(new MovementPoint(1150, 960, 0));
        points3.add(new MovementPoint(1700, 740,  Math.toRadians(-55), 50));
        paths.add(new PurePursuit(points3));

        ArrayList<MovementPoint> points4 = new ArrayList();
        points4.add(new MovementPoint(1700, 680, 0));
        points4.add(new MovementPoint(1200, 1000, -Math.toRadians(45), 0));
        points4.add(new MovementPoint(1190, 2700, -Math.toRadians(90), 0));
        points4.add(new MovementPoint(1000, 2950, -Math.toRadians(90), 50));
        paths.add(new PurePursuit(points4));

        ArrayList<MovementPoint> points5 = new ArrayList();
        points5.add(new MovementPoint(1000, 2950, 0));
        points5.add(new MovementPoint(1300, 2800, -Math.PI / 2, 0));
        points5.add(new MovementPoint(1225, 2000, -Math.PI / 2, 0));
        points5.add(new MovementPoint(1175, 800, -Math.PI / 2, 0));
        points5.add(new MovementPoint(1125, 550, -Math.PI / 2, 0));
        points5.add(new MovementPoint(1800, 340,  Math.toRadians(-30), 50));
        points5.add(new MovementPoint(1850, 330,  Math.toRadians(-80), 50));
        paths.add(new PurePursuit(points5));

        ArrayList<MovementPoint> points6 = new ArrayList();
        points6.add(new MovementPoint(1850, 330, 0));
        points6.add(new MovementPoint(1275, 1000, 0));
        points6.add(new MovementPoint(1300, 2000, -Math.toRadians(90), 0));
        points6.add(new MovementPoint(1600, 2900, Math.toRadians(-90), 50));
        paths.add(new PurePursuit(points6));

        ArrayList<MovementPoint> points7 = new ArrayList();
        points7.add(new MovementPoint(1600, 2800, 0));
        points7.add(new MovementPoint(1100, 2600, Math.toRadians(-90), 0));
        points7.add(new MovementPoint(1300, 1900, Math.toRadians(-90), 50));
        paths.add(new PurePursuit(points7));
    }

    public ArrayList<PurePursuit> getPaths(){
        return paths;
    }

}
