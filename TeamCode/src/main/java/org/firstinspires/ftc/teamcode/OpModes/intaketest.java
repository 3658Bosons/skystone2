package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

import org.firstinspires.ftc.teamcode.Hardware.Motor;
import org.openftc.revextensions2.ExpansionHubEx;

@TeleOp(name="intaketest", group="TeleOp")
public class intaketest extends LinearOpMode
{

    private Motor left;
    private Motor right;
    ExpansionHubEx expansionHub;

    @Override
    public void runOpMode()
    {
        left = new Motor("leftIn", this);
        right = new Motor("rightIn", this);
        left.setConstants(DcMotor.RunMode.RUN_WITHOUT_ENCODER, DcMotor.ZeroPowerBehavior.FLOAT, DcMotorSimple.Direction.FORWARD);
        right.setConstants(DcMotor.RunMode.RUN_WITHOUT_ENCODER, DcMotor.ZeroPowerBehavior.FLOAT, DcMotorSimple.Direction.FORWARD);

        expansionHub = hardwareMap.get(ExpansionHubEx.class, "Expansion Hub 2");

        waitForStart();

        while(opModeIsActive())
        {
            left.setPower(-gamepad1.right_stick_y);
            right.setPower(gamepad1.right_stick_y);

            telemetry.clear();
            telemetry.addLine("Total Current Draw (A): " + expansionHub.getTotalModuleCurrentDraw(ExpansionHubEx.CurrentDrawUnits.AMPS));
            telemetry.addLine("Left Motor Draw (A): " + expansionHub.getMotorCurrentDraw((ExpansionHubEx.CurrentDrawUnits.AMPS), 2));
            telemetry.addLine("Right Motor Draw (A): " + expansionHub.getMotorCurrentDraw((ExpansionHubEx.CurrentDrawUnits.AMPS), 3));
            telemetry.addLine("Voltage: " + expansionHub.read12vMonitor(ExpansionHubEx.VoltageUnits.VOLTS)); //Battery voltage
            telemetry.update();

        }

    }

}
