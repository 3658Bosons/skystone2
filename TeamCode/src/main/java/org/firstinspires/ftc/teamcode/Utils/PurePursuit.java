package org.firstinspires.ftc.teamcode.Utils;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import org.firstinspires.ftc.teamcode.Hardware.DriveTrain;

import java.util.ArrayList;

public class PurePursuit //class declaration
{

    private ArrayList<MovementPoint> points; //array of points
    private int currentPoint; //current point in iteration

    public PurePursuit(ArrayList<MovementPoint> points)
    {
        this.points = points;
        this.currentPoint = 1;
    }

    public boolean purePursuit(DriveTrain dt, State state, OpMode op, double speed, double turnSpeed, double lookahead, int reverse)
    {
        op.telemetry.addLine("point: " + currentPoint);//printing current point to phone
        if(currentPoint == points.size() - 1) //Last point
        {
            RobotMovement.setPoint(points.get(currentPoint), speed, turnSpeed); //set point to last point
            return RobotMovement.driveTowardPoint(dt, state, op.telemetry, true, reverse);//drive towards last point and return result
        }
        else
        {
            double[] intersections1 = getIntersections(points.get(currentPoint - 1), points.get(currentPoint), dt.getX(), dt.getY(), lookahead); //first intersect
            double[] intersections2 = getIntersections(points.get(currentPoint), points.get(currentPoint + 1), dt.getX(), dt.getY(), lookahead); //second intersect

            if(intersections2[0] > -1 || intersections2[2] > -1)
            {
                currentPoint++;
            }
            else if(intersections1[0] > -1 || intersections1[2] > -1)
            {
                if((Math.abs(intersections1[0] - points.get(currentPoint).getX()) < Math.abs(intersections1[2] - points.get(currentPoint).getX()) && intersections1[0] > -1) || intersections1[2] <= -1)
                {
                    RobotMovement.setPoint(new MovementPoint(intersections1[0], intersections1[1], points.get(currentPoint).getTheta(), 0), speed, turnSpeed);
                }
                else
                {
                    RobotMovement.setPoint(new MovementPoint(intersections1[2], intersections1[3], points.get(currentPoint).getTheta(), 0), speed, turnSpeed);
                }
            }
            else
            {
                RobotMovement.setPoint(points.get(currentPoint), speed, turnSpeed);
            }
        }
        RobotMovement.driveTowardPoint(dt, state, op.telemetry,false, reverse);
        return false;
    }

    public int getCurrentPoint(){
        return currentPoint;
    }

    public double[] getIntersections(MovementPoint p1, MovementPoint p2, double x, double y, double r) //x1,y1,x2,y2;
    {//math what are you a nerd haha
        double x1 = p1.getX() - x;
        double x2 = p2.getX() - x;
        double y1 = p1.getY() - y;
        double y2 = p2.getY() - y;

        double m = (y2 - y1) / (x2 - x1);
        double sqrt = -(m * m * x1 * x1) + (2 * m * x1 * y1) + (m * m * r * r) + (r * r) - (y1 * y1);

        if(sqrt < 0){
            return new double[]{-1, -1, -1, -1};
        }

        double xi1 = ((m * m * x1) - (m * y1) + Math.sqrt(sqrt)) / (m * m + 1);
        double xi2 = ((m * m * x1) - (m * y1) - Math.sqrt(sqrt)) / (m * m + 1);

        double yi1 = m*(xi1 - x1) + y1 + y;
        double yi2 = m*(xi2 - x1) + y1 + y;
        xi1 += x;
        xi2 += x;
        //THE KNIGHTS OF LAMBDA WILL RISE
        if(!((xi1 - x >= x1 && xi1 - x <= x2) || (xi1 - x <= x1 && xi1 - x >= x2)))
        {
            xi1 = -1;
            yi1 = -1;
        }
        if(!((xi2 - x >= x1 && xi2 - x <= x2) || (xi2 - x <= x1 && xi2 - x >= x2)))
        {
            xi2 = -1;
            yi2 = -1;
        }

        return new double[]{xi1, yi1, xi2, yi2};
    }

}
