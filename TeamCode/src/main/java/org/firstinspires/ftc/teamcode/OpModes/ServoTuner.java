package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.Hardware.DriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Gyro;
import org.firstinspires.ftc.teamcode.Utils.State;

@TeleOp(name="Servo Tuner", group="TeleOp")
public class ServoTuner extends LinearOpMode
{

    private Servo left;
    private Servo right;

    double l = .5;
    double r = .5;

    @Override
    public void runOpMode()
    {

        left = hardwareMap.get(Servo.class, "front");
        right = hardwareMap.get(Servo.class, "back");

        left.setPosition(.5);
        right.setPosition(.5);

        waitForStart();

        while(opModeIsActive())
        {

            l += (gamepad1.left_stick_y) / 1000;
            r += (gamepad1.right_stick_y) / 1000;

            left.setPosition(l);
            right.setPosition(r);

            telemetry.clear();
            telemetry.addLine("front: " + l);
            telemetry.addLine("back: " + r);
            telemetry.update();
        }

    }

}
