package org.firstinspires.ftc.teamcode.OpModes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.teamcode.Hardware.Deposit;
import org.firstinspires.ftc.teamcode.Hardware.DriveTrain;
import org.firstinspires.ftc.teamcode.Hardware.Gyro;
import org.firstinspires.ftc.teamcode.Hardware.Intake;
import org.firstinspires.ftc.teamcode.Hardware.Motor;
import org.firstinspires.ftc.teamcode.Utils.State;
import org.openftc.revextensions2.ExpansionHubEx;

@TeleOp(name="TeleOp", group="TeleOp")
public class Tele extends LinearOpMode
{

    private DriveTrain dt;
    private Intake intake;
    private Deposit deposit;
    private State state;

    private boolean lastR;
    private boolean lastL;
    private boolean lastUp;
    private boolean lastDown;
    private boolean lastLeft;

    @Override
    public void runOpMode()
    {
        state = State.DRIVING;

        lastR = false;
        lastL = false;
        lastUp = false;
        lastDown = false;
        lastLeft = false;

        waitForStart();

        dt = new DriveTrain(this, true);
        intake = new Intake(this);
        deposit = new Deposit(this, state, true);

        deposit.tick(0, State.LOCKED);
        deposit.tick(0, State.DRIVING);

        while(opModeIsActive())
        {
            double x = gamepad1.left_stick_x * 1.5;
            double y = -gamepad1.left_stick_y;
            double turn = gamepad1.right_stick_x;

            intake.power(gamepad1.right_trigger - gamepad1.left_trigger);

            boolean r = gamepad1.right_bumper;
            boolean l = gamepad1.left_bumper;
            boolean up = gamepad2.dpad_up;
            boolean down = gamepad2.dpad_down;
            boolean left = gamepad1.dpad_left || gamepad2.dpad_left;

            if(gamepad1.a){
                dt.grabFoundation();
            }else if(gamepad1.y){
                dt.releaseFoundation();
            }else if(gamepad1.b){
                dt.idleFoundation();
                deposit.grabStone();
            }

            switch(state)
            {
                case DRIVING:
                    if(r && !lastR)
                        state = State.LOCKED;
                    if(up && !lastUp){
                        deposit.incLevel(1);
                    }else if(down && !lastDown){
                        deposit.incLevel(-1);
                    }
                    if(left && !lastLeft){
                        if(deposit.getRotate()){
                            deposit.setRotate(false);
                        }else{
                            deposit.setRotate(true);
                        }
                    }
                    break;
                case LOCKED:
                    if(r && !lastR)
                        state = State.LIFTING;
                    if(gamepad1.x)
                        state = State.DRIVING;
                    if(up && !lastUp){
                        deposit.incLevel(1);
                    }else if(down && !lastDown){
                        deposit.incLevel(-1);
                    }
                    if(left && !lastLeft){
                        if(deposit.getRotate()){
                            deposit.setRotate(false);
                        }else{
                            deposit.setRotate(true);
                        }
                    }
                    break;
                case LIFTING:
                    if(r && !lastR){
                        state = State.DEPOSITING;
                    }
                    x /= 5;
                    y /= 1.5;
                    turn /= 3;
                    break;
                case DEPOSITING:
                    if(r && !lastR) {
                        state = State.RETRACTING;
                    }
                    x /= 5;
                    y /= 3.5;
                    turn /= 3;
                    break;
                case RETRACTING:
                    break;
            }

            double p = Math.sqrt((x * x) + (y * y));
            double theta = Math.atan2(y, x);

            dt.drive(p, theta, turn, state);

            double lift;
            double power = -gamepad2.right_stick_y;

            if(l && !lastL){
                deposit.capStoneThread();
            }

            if(power > 0){
                lift = power * 3;
            }else{
                lift = power * 2;
            }

            if(deposit.tick(((gamepad1.dpad_up ? 1 : 0)
                    - (gamepad1.dpad_down ? 1 : 0)) * 3 + lift, state)){
                state = State.DRIVING;
            }

            lastR = r;
            lastL = l;
            lastUp = up;
            lastDown = down;
            lastLeft = left;
        }

    }

}
