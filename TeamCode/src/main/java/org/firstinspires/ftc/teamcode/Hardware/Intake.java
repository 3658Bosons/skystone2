package org.firstinspires.ftc.teamcode.Hardware;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;

public class Intake
{

    private Motor left;
    private Motor right;

    private static double MAX_POWER=.65;
    private static double MAX_EJECT = -.6;

    private LinearOpMode op;

    public Intake(LinearOpMode op)
    {
        left = new Motor("leftIn", op);
        right = new Motor("rightIn", op);

        left.setConstants(DcMotor.RunMode.RUN_WITHOUT_ENCODER, DcMotor.ZeroPowerBehavior.FLOAT, DcMotorSimple.Direction.FORWARD);
        right.setConstants(DcMotor.RunMode.RUN_WITHOUT_ENCODER, DcMotor.ZeroPowerBehavior.FLOAT, DcMotorSimple.Direction.FORWARD);
        this.op = op;
    }

    public void power(double power){ //positive is in
        if(Math.abs(power) > MAX_POWER)
        {
            power=MAX_POWER * (Math.abs(power) / power);
        }
        if(power < MAX_EJECT)
            power = MAX_EJECT;

        left.setPower(-power);
        right.setPower(power);
    }

    public void unjam(){
        Thread t = new Thread(unjamRoutine);
        t.start();
    }

    private Runnable unjamRoutine = new Runnable()
    {
        @Override
        public void run()
        {
            power(-1);
            op.sleep(200);
            power(1);
        }
    };

}
