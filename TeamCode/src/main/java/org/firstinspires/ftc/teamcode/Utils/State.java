package org.firstinspires.ftc.teamcode.Utils;

public enum State
{
    DRIVING,
    LOCKED,
    LIFTING,
    DEPOSITING,
    RETRACTING,
    BUFFER
}
